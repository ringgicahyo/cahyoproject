var clicked = true;
function f() {
    $(document).ready(function () {
        $('button').click(function () {
            if(clicked) {
                $('body').css('background-color', '#fefbd8')
                clicked = false;
            }else{
                $('body').css('background-color', '#d5f4e6')
                clicked = true;
            }
        });
    });
}

$ (document).ready(function(){
$( function() {
	$( "#accordion" ).accordion({
		active: false,
        collapsible: true,
		heightStyle: "content",
        clearStyle: true,
		});
	});
});

$(window).bind("load", function () {
    $('#work-in-progress').fadeOut(1500);
});

// var fav = 0;
// function bookmarking(id) {
//     var img = $('#' + id + ' td img');
//     var imgSource = img[1].src;
//
//     if (imgSource.includes('Unbookmark')) {
//         fav++;
//         img[1].src = '../static/images/Bookmarked.png';
//     } else {
//         fav--;
//         img[1].src = '../static/images/Unbookmark.png';
//     }
//     $('#fav').html('Total of My Favorite Books: ' + fav);
// }
//
// $.ajax('/request/')
//     .done(function (data) {
//         data = data['books'];
//         var tbody = '';
//         for (var i = 0; i < data.length; i++) {
//             tbody = tbody
//                 + '<tr id=\"'+ data[i].id +'\" style="text-align: center">'
//                     + '<td class="align-middle"><img src=\"'+ data[i].image +'\"</td>'
//                     + '<td class="align-middle">' + data[i].title + '</td>'
//                     + '<td class="align-middle">' + data[i].author + '</td>'
//                     + '<td class="align-middle"><img onclick=\"bookmarking(\''+ data[i].id +'\')\" src=\"../static/images/Unbookmark.png\" style="width: 40px;"></td>';
//         }
//         $(tbody).appendTo('#tableBody');
//     });

$(document).ready(function () {
    $.ajax({
        method: "GET",
        url: "/getdata",
        success: function (hasil) {
            var data = hasil.data;
            for (var x = 0; x < data.length; x++) {
                var dataBook = data[x];
                var title = dataBook.title;
                if (dataBook.boolean) {
                    var button = '<button' + ' id="' + dataBook.id + '"' + ' class="bttnn"' + ' onClick="deleteFavorite(\'' + dataBook.id + '\')">' +'<i class="del"></i></button>';
                } else {
                    var button = '<button'+ ' id="' + dataBook.id + '"' + ' class="bttnn"' + ' onClick="addFavorite(\'' + dataBook.id + '\')">' +'<i class="ic"></i></button>';
                }
                var html = '<tr class="table-info">' + '<td>' + button + title + '</td>' + '<td>' + dataBook.authors + '</td>' +
                    '<td>' + dataBook.published + '</td>' + '</tr>';
                $('tbody').append(html);
            }
        },
        error: function (error) {
            alert("Halaman buku tidak ditemukan")
        }
    })
});

var addFavorite = function (id) {
    var csrftoken = $("[name=csrfmiddlewaretoken]").val();
    $.ajax({
        method: "POST",
        url: "/library/",
        headers:{
        "X-CSRFToken": csrftoken
    },
        data: {id: id},
        success: function (hasil) {
        var button = '<button' + ' id="' + hasil.id + '"' + ' class="bttnn"' + ' onClick="deleteFavorite(\'' + hasil.id + '\')">' +'<i class="del"></i></button>';
            $("#"+hasil.id).replaceWith(button);
            $(".count").replaceWith("<span class='count'>" + hasil.count + "</span>")
        },
        error: function (error) {
            alert("Error, cannot get data from server")
        }
    });
};
var deleteFavorite = function (id) {
    var csrftoken = $("[name=csrfmiddlewaretoken]").val();
    $.ajax({
        method: "POST",
        url: "/delete",
        headers:{
        "X-CSRFToken": csrftoken
    },
        data: {id: id},
        success: function (count) {
            var button = '<button' + ' id="' + id + '"' + ' class="bttnn"' + ' onClick="addFavorite(\'' + id + '\')">' +'<i class="ic"></i></button>';
            $("tbody").find("#"+id).replaceWith(button);
            $(".count").replaceWith("<span class='count'>" + count.count + "</span>");
        },
        error: function (error) {
            alert("Error, cannot get data from server")
        }
    });
};

var request = function (query) {
    var csrftoken = $("[name=csrfmiddlewaretoken]").val();
    $.ajax({
        method: "POST",
        url: "/getdata",
        headers: {
            "X-CSRFToken": csrftoken
        },
        data: {query: query},
        success: function (hasil) {
            $(".table-info").remove();
            var data = hasil.data;
            for (var x = 0; x < data.length; x++) {
                var dataBook = data[x];
                var title = dataBook.title;
                if (dataBook.boolean) {
                    var button = '<button' + ' id="' + dataBook.id + '"' + ' class="bttnn"' + ' onClick="deleteFavorite(\'' + dataBook.id + '\')">' + '<i class="del"></i></button>';
                } else {
                    var button = '<button' + ' id="' + dataBook.id + '"' + ' class="bttnn"' + ' onClick="addFavorite(\'' + dataBook.id + '\')">' + '<i class="ic"></i></button>';
                }
                var html = '<tr class="table-info">' + '<td>' + button + title + '</td>' + '<td>' + dataBook.authors + '</td>' +
                    '<td>' + dataBook.published + '</td>' + '</tr>';
                $('tbody').append(html);
            }
        }
    })
};

$("#programming").on("click", function () {
    request("programming")
});

$("#quilting").on("click", function () {
    request("quilting")
});

$("#django").on("click", function () {
    request("django")
});

$("#python").on("click", function () {
    request("python")
});

// $(document).ready(function () {
//        gapi.load('auth2', function() {
//         gapi.auth2.init();
//       });
// });
