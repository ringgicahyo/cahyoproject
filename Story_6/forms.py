from django import forms

class statusPage(forms.Form):
    status = forms.CharField(label="What's your status?", max_length=300, widget=forms.TextInput(attrs={'class': 'form-control'}))
