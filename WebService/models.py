from django.db import models
from django.contrib.auth.models import User

class subscriberModels(models.Model):
    name = models.CharField(max_length=100)
    email = models.EmailField(max_length=50, unique=True)
    password = models.CharField(max_length=20)
