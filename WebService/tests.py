from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import *

class SubscriberUnitTest(TestCase):

    def test_subscribe_url_exists(self):
        response = Client().get('/subscribe/')
        self.assertEqual(response.status_code, 200)

    def test_subscribe_func(self):
        found = resolve('/subscribe/')
        self.assertEqual(found.func, subscribe)

    def test_create_object_model(self):
        subscriberModels.objects.create(name='Ringgi', email='ringgicahyo@gmail.com', password='remember it')
        counting_object_status = subscriberModels.objects.all().count()
        self.assertEqual(counting_object_status, 1)

    def test_subcribe_post_success_and_give_feedback(self):
        name = 'test'
        email = 'test@email.com'
        password = 'testpassword'
        response_post = Client().post('/subscribe/', {'name': name, 'email': email, 'password': password})
        self.assertEqual(response_post.status_code, 200)
        self.assertTrue(subscriberModels.objects.filter(email=email).exists())

    def test_subscribe_email_is_not_valid(self):
        name = 'test'
        email = 'testdfdsfsfs'
        password = 'testpassword'
        response_post = Client().post('/subscribe/', {'name': name, 'email': email, 'password': password})
        self.assertEqual(response_post.status_code, 200)
