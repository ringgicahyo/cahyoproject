import time
from django.test import TestCase
from django.test import Client
from .models import statusModels
from .forms import statusPage
import requests
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

# Create your tests here.
class Story6UnitTest(TestCase):

    def test_story_6_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_create_object_model(self):
        statusModels.objects.create(status='Saya baik-baik saja')
        counting_object_status = statusModels.objects.all().count()
        self.assertEqual(counting_object_status, 1)

    def test_form_validation_for_blank_items(self):
        form = statusPage(data={'status': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['status'],
            ["This field is required."])

    def test_story6_post_success_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/post/', {'status': test})
        self.assertEqual(response_post.status_code, 302)

        response = Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_story6_post_error_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/', {'status': ''})
        self.assertEqual(response_post.status_code, 200)

        response = Client().get('/status/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)

    def test_story6_delete_database(self):
        test = 'Anonymous'
        response_post = Client().delete('/')
        self.assertEqual(response_post.status_code, 200)

        response = Client().get('/delete/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)

    def test_story6_blog_page(self):
        response = Client().get('/profile/')
        self.assertEqual(response.status_code, 200)

    def test_library_page_exists(self):
        response = Client().get('/library/')
        self.assertEqual(response.status_code, 200)

    def test_json_library(self):
        response = Client().get('/request/')
        self.assertEqual(response.status_code, 200)

    # def test_search_post_success_and_render_the_result(self):
    #     response_post = Client().post('/request/', {'keywords': 'quilting'})
    #     self.assertEqual(response_post.status_code, 200)
    #
    #     response = Client().get('/request/')
    #     html_response = response.content.decode('utf8')
    #     self.assertIn('quilting', html_response)

# class Story6FunctionalTest(TestCase):
#
#     def setUp(self):
#         chrome_options = Options()
#         self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
#         super(Story6FunctionalTest, self).setUp()
#
#     def tearDown(self):
#         self.selenium.quit()
#         super(Story6FunctionalTest, self).tearDown()
#
#     def test_input_coba_coba(self):
#         selenium = self.selenium
#         selenium.get('http://ringgicahyo.herokuapp.com/')
#         time.sleep(3)
#         status = selenium.find_element_by_id('id_status')
#         status.send_keys('Coba Coba')
#         status.send_keys(Keys.RETURN)
#         time.sleep(3)
#         self.assertIn('Coba Coba', self.selenium.page_source)
#
#     def test_navbar_item_font_size_css(self):
#         selenium = self.selenium
#         selenium.get('http://ringgicahyo.herokuapp.com/')
#         time.sleep(3)
#         nav_item = selenium.find_element_by_class_name('nav-item')
#         nav_item_margin = nav_item.value_of_css_property('margin')
#         nav_item_font_family = nav_item.value_of_css_property('font-family')
#         nav_item_font_weight = nav_item.value_of_css_property('font-weight')
#         nav_item_font_size = nav_item.value_of_css_property('font-size')
#         time.sleep(3)
#         self.assertEqual(nav_item_margin, '8px')
#         self.assertEqual(nav_item_font_family, 'Quintessential')
#         self.assertEqual(nav_item_font_weight, '700')
#         self.assertEqual(nav_item_font_size, '18px')
#
#     def test_form_block_background_css(self):
#         selenium = self.selenium
#         selenium.get('http://ringgicahyo.herokuapp.com/')
#         time.sleep(3)
#         form_block = selenium.find_element_by_class_name('form-block')
#         form_block_border_style = form_block.value_of_css_property('border-style')
#         time.sleep(3)
#         self.assertEqual(form_block_border_style, 'ridge')
#
#     def test_navbar_item_margin_css(self):
#         selenium = self.selenium
#         selenium.get('http://ringgicahyo.herokuapp.com/')
#         time.sleep(3)
#         nav_item = selenium.find_element_by_class_name('nav-item')
#         nav_item_location = nav_item.location
#         nav_item_size = nav_item.size
#         time.sleep(3)
#         self.assertEqual(nav_item_location['x'], 795)
#         self.assertEqual(nav_item_location['y'], 14)
#         self.assertEqual(nav_item_size['height'], 51)
#         self.assertEqual(nav_item_size['width'], 93)
#
#     def test_form_block_position_css(self):
#         selenium = self.selenium
#         selenium.get('http://ringgicahyo.herokuapp.com/')
#         time.sleep(3)
#         form_block = selenium.find_element_by_class_name('form-block')
#         form_block_location = form_block.location
#         form_block_size = form_block.size
#         time.sleep(3)
#         self.assertEqual(form_block_location['x'], 200)
#         self.assertEqual(form_block_location['y'], 153)
#         self.assertEqual(form_block_size['height'], 219)
#         self.assertEqual(form_block_size['width'], 636)