import json
from django.shortcuts import render
from .models import subscriberModels
from .forms import subscriberPage
from django.http import HttpResponse
from django.http import JsonResponse


def subscribe(request):
    response = {}
    if request.method == 'POST':
        subscriber = subscriberPage(request.POST)
        if subscriber.is_valid():
            response['name'] = request.POST['name']
            response['email'] = request.POST['email']
            response['password'] = request.POST['password']
            subscriber_database = subscriberModels(name=request.POST['name'], email=request.POST['email'],
                                    password=request.POST['password'])
            subscriber_database.save()
            response['message'] = 'Data berhasil disimpan'
            response['success'] = True
            json_data = json.dumps(response)
            return HttpResponse(json_data, content_type='application/json')
        else:
            subscriber.clean()
            response['message'] = 'Data tidak berhasil disimpan'
            response['success'] = False
            json_data = json.dumps(response)
            return HttpResponse(json_data, content_type='application/json')
    else:
        subscriber = subscriberPage()
        return render(request, 'Subscribe.html', {'form': subscriber})


def validate_email(request):
    email = request.GET.get('email', None)
    data = {
        'is_taken': subscriberModels.objects.filter(email=email).exists()
    }
    if data['is_taken']:
        data['error_message'] = 'Email tersebut sudah pernah didaftarkan sebelumnya, silahkan daftar dengan email lain.'
    return JsonResponse(data)

def getDataSubscribers(request):
    subscribers = subscriberModels.objects.all()
    data = list()
    for i in subscribers:
        result = {'name': i.name, 'email': i.email}
        data.append(result)
    return JsonResponse(data, safe=False)

def unsubscribe(request):
    if request.method == "POST":
        email = request.POST['email']
        subscriberModels.objects.get(email=email).delete()
        json_data = json.dumps({'message': 'Berhasil unsubscribe!'})
        return HttpResponse(json_data, content_type='application/json')
    else:
        subscriber = subscriberPage()
        return render(request, 'Subscribe.html', {'form': subscriber})


