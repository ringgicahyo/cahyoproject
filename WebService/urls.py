from django.urls import path
from .views import *
from django.conf.urls import url

app_name = 'subscribe'
urlpatterns = [
    path('subscribe/', subscribe, name='subscribe'),
    url(r'^ajax/validate_email/$', validate_email, name='validate_email'),
    url(r'^list_subscribers', getDataSubscribers, name='list_subscribers'),
    url(r'^unsubscribe', unsubscribe, name='unsubscribe'),

]
