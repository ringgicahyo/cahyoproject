from django.shortcuts import render
from django.http import JsonResponse, HttpResponseRedirect
from django.urls import reverse

from .forms import statusPage
from .models import statusModels
import requests

# Create your views here.
def statusView(request):
    response = {}
    html = 'Story_6.html'
    response['form'] = statusPage
    result = statusModels.objects.all().order_by('date')
    response['result'] = result
    return render(request, html, response)


def status_result(request):
    result = statusModels.objects.all().order_by('date')
    response = {'result': result}
    html = 'Story_6.html'
    return render(request, html, response)

def delete_database(request):
    delete = statusModels.objects.all().delete()
    response = {'delete': delete, 'form': statusPage}
    html = 'Story_6.html'
    return render(request, html, response)

def profile_view(request):
    return render(request, "Profile.html")

def postView(request):
    response = {}
    stats = statusPage(request.POST or None)
    if (request.method == 'POST' and stats.is_valid()):
        response['status'] = request.POST['status']
        status_form = statusModels(status=response['status'])
        status_form.save()
    return HttpResponseRedirect('/')

# key = "quilting"
# def library(request):
#     books = list()
#     keywords = key
#     response = requests.get('https://www.googleapis.com/books/v1/volumes?q=' + keywords)
#     data = response.json()
#     for i in range(len(data['items'])):
#         books.append({
#             'id': data['items'][i]['id'],
#             'image': data['items'][i]['volumeInfo']['imageLinks']['thumbnail'],
#             'title': data['items'][i]['volumeInfo']['title'],
#             'author': data['items'][i]['volumeInfo']['authors'][0],
#         })
#     return JsonResponse({'books': books})
#
# def libraryView(request):
#     if request.method == "POST":
#         response = request.POST.get('keywords')
#         global key
#         key = response
#         return render(request, 'Library.html')
#     else:
#         return render(request, 'Library.html')

def index(request):
    if 'user_id' not in request.session:
        return HttpResponseRedirect(reverse('login'))
    if request.method == 'POST':
        id = request.POST['id']
        if not 'book' in request.session.keys():
            request.session['book'] = [id]
            size = 1
        else:
            books = request.session['book']
            books.append(id)
            request.session['book'] = books
            size = len(books)
        return JsonResponse({'count': size, 'id': id})
    if 'book' in request.session.keys():
        listbookSession = request.session['book']
    else:
        listbookSession = []
    nama = request.session['name']
    return render(request, 'Library.html', {'count': len(listbookSession), 'nama': nama})


def getData(request):
    if 'user_id' not in request.session:
        return HttpResponseRedirect(reverse('login'))
    url = "https://www.googleapis.com/books/v1/volumes?q=quilting"
    if request.method == "POST":
        query = request.POST['query']
        url = "https://www.googleapis.com/books/v1/volumes?q=" + query
    get = requests.get(url).json()
    hasil = get['items']
    newlist = []
    for items in hasil:
        dataId = items['id']
        if dataId in request.session['book']:
            bool = True
        else:
            bool = False
        newdict = {"title": items['volumeInfo']['title'], "authors": items['volumeInfo']['authors'],
                   "published": items['volumeInfo']['publishedDate'],
                   'id': items['id'], 'boolean': bool}
        newlist.append(newdict)
    return JsonResponse({'data': newlist})


def delete(request):
    if 'user_id' not in request.session:
        return HttpResponseRedirect(reverse('login'))
    if request.method == 'POST':
        id = request.POST['id']
        books = request.session['book']
        books.remove(id)
        size = len(books)
        request.session['book'] = books
        return JsonResponse({'count': size})
