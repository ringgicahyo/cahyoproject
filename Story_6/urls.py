from django.urls import path
from .views import *

urlpatterns = [
    path('post/', postView, name='post'),
    path('', statusView, name='statusView'),
    path('status/', status_result, name='result'),
    path('delete/', delete_database, name='delete'),
    path('profile/', profile_view, name='profile'),
    # path('library/', libraryView, name='library'),
    # path('request/', library),
    path('library/', index, name='library'),
    path('delete', delete),
    path('getdata', getData),

]