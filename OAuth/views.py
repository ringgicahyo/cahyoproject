from django.shortcuts import render
from google.oauth2 import id_token
from google.auth.transport import requests
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.http import JsonResponse


def login(request):
    if request.method == "POST":
        try:
            token = request.POST['id_token']
            idinfo = id_token.verify_oauth2_token(
                token, requests.Request(), "632242128761-pba3bde883snq2ee4bmv8ck9sqm5p6gf.apps.googleusercontent.com")
            if idinfo['iss'] not in ['accounts.google.com', 'https://accounts.google.com']:
                raise ValueError('Wrong issuer.')
            userid = idinfo['sub']
            email = idinfo['email']
            name = idinfo['name']
            request.session['user_id'] = userid
            request.session['email'] = email
            request.session['name'] = name
            request.session['book'] = []
            return JsonResponse({"status": "0", 'url': reverse("login")})
        except ValueError:
            return JsonResponse({"status": "1"})
    return render(request, 'Login.html')


def logout(request):
    request.session.flush()
    return HttpResponseRedirect(reverse('login'))

