function disableSubmitButton(){
    if ($('#id_name').val().length && $('#id_email').val().length && $('#id_password').val().length > 0 &&
        email_is_validated === true) {
        $("input[type=submit]").prop("disabled", false);
    }
    else {
        $("input[type=submit]").prop("disabled", true);
    }
}

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

function csrfSafeMethod(method) {
    return(/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}

var csrftoken = getCookie('csrftoken');
var email_is_validated = false;

$(document).ready(function () {
    disableSubmitButton();
    $('#id_name, #id_email, #id_password').on('change',function(){
        disableSubmitButton();
    });
    $.ajaxSetup({
        beforeSend: function (xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader('X-CSRFToken', csrftoken);
            }
        }
    });

    $("#id_email").on({
        'change': function () {
            var email = $(this).closest("form");
            $.ajax({
                url: email.attr("data-validate-email-url"),
                data: email.serialize(),
                dataType: 'json',
                success: function (data) {
                    if (data.is_taken) {
                        alert(data.error_message);
                    }
                }
            })
            .then(function (data) {
                email_is_validated = !(data.is_taken);
            });
        },
    });

    $('#subscribeMe').on('submit', function (e) {
        e.preventDefault();
        console.log("Form is submitted!");
        var name = $('#id_name');
        var email = $('#id_email');
        var password = $('#id_password');
        var data = {
            'name': name.val(),
            'email': email.val(),
            'password': password.val(),
        };
        console.log(data);
        $.ajax({
            method: 'POST',
            url: '/subscribe/',
            dataType: 'json',
            data: data,
            success: function (data) {
                if (data.success) {
                    console.log("Form is success!");
                    alert(data.message);
                }
                else {
                    console.log("Form is not success!");
                    alert(data.message);
                }
            }
        }).then(function (data) {
            if (data.success) {
                name.val("");
                email.val("");
                password.val("");
            }
        })
    });
    $(window).load(function () {
        $.ajax({
            method: "GET",
            url: '/list_subscribers/',
            success: function (data) {
                if (data.length > 0){
                    var tbody = '';
                    for (let i = 0; i < data.length; i++){
                        var name = data[i]['name'];
                        var email = data[i]['email'];
                        tbody = tbody
                                + '<tr>'
                                + '<td class="align-middle">' + name + '</td>'
                                + '<td class="align-middle">' + email + '</td>'
                                + '<td class="align-middle">'
                                + '<button type="submit" class="btn-danger unsubscribe" data-email=' + email + '>'
                                + 'Unsubscribe</button></td>'
                    }
                    $(tbody).appendTo('#tableSubscriber');
                }
            }
        })
    });

    $('#tableSubscriber').on('click', 'td .unsubscribe', function (e) {
        e.preventDefault();
        var email = $(this).attr('data-email');
        $.ajax({
            url: '/unsubscribe/',
            method: 'POST',
            dataType: 'json',
            data: {'email': email},
            success: function () {
                location.reload();
            }
        })
    })
});