from django import forms

class subscriberPage(forms.Form):
    name = forms.CharField(label='Name', max_length=100, required=True,
                           widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Enter Your Name'}))
    email = forms.EmailField(label='Email', max_length=50, required=True,
                             widget=forms.EmailInput(
                                 attrs={'class': 'form-control', 'placeholder': 'Enter Your E-mail'}))
    password = forms.CharField(label='Password', max_length=20, required=True,
                               widget=forms.PasswordInput(
                                   attrs={'class': 'form-control', 'placeholder': 'Enter Your Password'}))

    # def clean_email(self):
    #     email = self.cleaned_data.get('email')
    #     try:
    #         subscriberModels.objects.get(email=email)
    #     except subscriberModels.DoesNotExist:
    #         return email
    #     raise ValidationError('Email exists. Please use another email.')
